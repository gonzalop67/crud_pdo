<?php

require_once "Conexion.php";

class Crud extends Conexion
{
	protected $conexion;

	public function __construct()
	{
		$this->conexion = parent::conectar();
	}

	public function mostrarDatos()
	{
		$sql = "SELECT id,
					   nombre,
					   sueldo,
					   edad,
					   fRegistro
				  FROM t_crud";
		$query = $this->conexion->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function insertarDatos($datos)
	{
		$sql = "INSERT INTO t_crud (nombre, sueldo, edad, fRegistro)
							VALUES(:nombre, :sueldo, :edad, :fRegistro)";
		$query = $this->conexion->prepare($sql);
		$query->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$query->bindParam(":sueldo", $datos["sueldo"], PDO::PARAM_STR);
		$query->bindParam(":edad", $datos["edad"], PDO::PARAM_INT);
		$query->bindParam(":fRegistro", $datos["fecha"], PDO::PARAM_STR);

		return $query->execute();
	}

	public function obtenerDatos($id)
	{
		$sql = "SELECT id,
					   nombre,
					   sueldo,
					   edad,
					   fRegistro
   				  FROM t_crud
				 WHERE id = :id";
		$query = $this->conexion->prepare($sql);
		$query->bindParam(":id", $id, PDO::PARAM_INT);
		$query->execute();
		return $query->fetch();
	}

	public function actualizarDatos($datos){
		$sql = "UPDATE t_crud 
				   SET nombre = :nombre,
					   sueldo = :sueldo,
					   edad = :edad,
					   fRegistro = :fRegistro
				 WHERE id = :id";
		$query = $this->conexion->prepare($sql);

		$query->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$query->bindParam(":sueldo", $datos["sueldo"], PDO::PARAM_STR);
		$query->bindParam(":edad", $datos["edad"], PDO::PARAM_INT);
		$query->bindParam(":fRegistro", $datos["fecha"], PDO::PARAM_STR);
		$query->bindParam(":id", $datos["id"], PDO::PARAM_INT);

		return $query->execute();
	}

	public function eliminarDatos($id){
		$sql = "DELETE FROM t_crud WHERE id = :id";
		$query = $this->conexion->prepare($sql);
		$query->bindParam(":id", $id, PDO::PARAM_INT);

		return $query->execute();
	}
}
